'use strict';

var gulp = require('gulp'),
    watch = require('gulp-watch'),
    prefixer = require('gulp-autoprefixer'),
    uglify = require('gulp-uglify'),
    sass = require('gulp-sass'),
    sourcemaps = require('gulp-sourcemaps'),
    rigger = require('gulp-rigger'),
    cleanCSS = require('gulp-clean-css'),
    rimraf = require('rimraf'),
    browserSync = require("browser-sync"),
    ejs = require('gulp-ejs');

    var reload = browserSync.reload;

    var path = {
        build: {
            html: 'dist/',
            js: 'dist/js/',
            css: 'dist/css/',
            img: 'dist/img/',
            fonts: 'dist/fonts/'
        },
        src: {
            ejs: 'assets/*.ejs',
            js: 'assets/js/*.js',
            style: 'assets/sass/*.scss',
            img: 'assets/img/**/*.*',
            fonts: 'assets/fonts/**/*.*'
        },
        watch: {
            html: 'assets/**/*.ejs',
            js: 'assets/js/**/*.js',
            style: 'assets/sass/**/*.scss',
            img: 'assets/img/**/*.*',
            fonts: 'assets/fonts/**/*.*'
        },
        clean: './dist'
    };
    
    var config = {
        server: {
            baseDir: "./dist"
        },
        tunnel: true,
        host: 'localhost',
        port: 9000,
        logPrefix: "Frontend_Devil"
    };
    
    gulp.task('webserver', function () {
        browserSync(config);
    });
    
    gulp.task('clean', function (cb) {
        rimraf(path.clean, cb);
    });
    
    gulp.task('ejs:build', function () {
        gulp.src(path.src.ejs)
            .pipe(rigger())
            .pipe(ejs({ msg: 'Hello Gulp!'}, {}, { ext: '.html' }))
            .pipe(gulp.dest(path.build.html))
            .pipe(reload({stream: true}));
    });
    
    gulp.task('js:build', function () {
        gulp.src(path.src.js)
            .pipe(rigger())
            .pipe(sourcemaps.init())
            // .pipe(uglify())
            .pipe(sourcemaps.write())
            .pipe(gulp.dest(path.build.js))
            .pipe(reload({stream: true}));
    });
    
    gulp.task('style:build', function () {
        gulp.src(path.src.style)
            .pipe(sourcemaps.init())
            .pipe(sass({
                sourceMap: true,
                errLogToConsole: true
            }).on('error', sass.logError))
            .pipe(prefixer({
                browsers: ['last 10 versions'],
                cascade: false
            }))
            .pipe(cleanCSS())
            .pipe(sourcemaps.write())
            .pipe(gulp.dest(path.build.css))
            .pipe(reload({stream: true}));
    });
    
    gulp.task('image:build', function () {
        gulp.src(path.src.img)
            .pipe(gulp.dest(path.build.img))
            .pipe(reload({stream: true}));
    });
    
    gulp.task('fonts:build', function() {
        gulp.src(path.src.fonts)
            .pipe(gulp.dest(path.build.fonts))
    });
    
    gulp.task('build', [
        'ejs:build',
        'js:build',
        'style:build',
        'fonts:build',
        'image:build'
    ]);
    
    
    gulp.task('watch', function(){
        watch([path.watch.html], function(event, cb) {
            gulp.start('ejs:build');
        });
        watch([path.watch.style], function(event, cb) {
            gulp.start('style:build');
        });
        watch([path.watch.js], function(event, cb) {
            gulp.start('js:build');
        });
        watch([path.watch.img], function(event, cb) {
            gulp.start('image:build');
        });
        watch([path.watch.fonts], function(event, cb) {
            gulp.start('fonts:build');
        });
    });
    
    
    gulp.task('default', ['build', 'webserver', 'watch']);
$(document).ready(function () {

  // change files url
  function changeUrl(tagName, attr) {

    for (var i = 0; i < document.getElementsByTagName(tagName).length; i++) {
      var url = document.getElementsByTagName(tagName)[i].getAttribute(attr);
      if (url && url.indexOf('http://') === -1 && url.indexOf('https://') === -1) {
        document.getElementsByTagName(tagName)[i][attr] = window.ngtheme + url;
      }
    }
  }

  changeUrl('img', 'src');
  changeUrl('script', 'src');
  changeUrl('link', 'href');

  //side-bar
  $('.menu').click(function (e) {
    e.preventDefault();
    $(this).toggleClass('active');
    if ($(this).hasClass('active')) {
      $(this).find('i').attr('class', 'fas fa-angle-up');
    } else {
      $(this).find('i').attr('class', 'fas fa-angle-down');
    }
  })
  $('.open-menu-items').click(function (e) {
    e.preventDefault();
    $(this).toggleClass('active');
    if ($(this).hasClass('active')) {
      $(this).find('i').attr('class', 'fas fa-angle-up');
    } else {
      $(this).find('i').attr('class', 'fas fa-angle-down');
    }
  })

  // modal

  $('.open-modal').click(function (e) {
    e.preventDefault();
    var id = $(this).attr('href');
    $(id).addClass('active');
  });

  $('.close').click(function (e) {
    e.preventDefault();
    $('.modal').removeClass('active');
  });

  $('.modal').click(function (e) {
    if ($(e.target).hasClass('modal') && $(e.target).hasClass('active')) {
      $(e.target).removeClass('active')
    }
  });

 // menu

 $('.toggle a').click(function (e) {
   e.preventDefault();
   $('.header-menu').toggleClass('active');
 })
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlcyI6WyJtYWluLmpzIl0sInNvdXJjZXNDb250ZW50IjpbIiQoZG9jdW1lbnQpLnJlYWR5KGZ1bmN0aW9uICgpIHtcblxuICAvLyBjaGFuZ2UgZmlsZXMgdXJsXG4gIGZ1bmN0aW9uIGNoYW5nZVVybCh0YWdOYW1lLCBhdHRyKSB7XG5cbiAgICBmb3IgKHZhciBpID0gMDsgaSA8IGRvY3VtZW50LmdldEVsZW1lbnRzQnlUYWdOYW1lKHRhZ05hbWUpLmxlbmd0aDsgaSsrKSB7XG4gICAgICB2YXIgdXJsID0gZG9jdW1lbnQuZ2V0RWxlbWVudHNCeVRhZ05hbWUodGFnTmFtZSlbaV0uZ2V0QXR0cmlidXRlKGF0dHIpO1xuICAgICAgaWYgKHVybCAmJiB1cmwuaW5kZXhPZignaHR0cDovLycpID09PSAtMSAmJiB1cmwuaW5kZXhPZignaHR0cHM6Ly8nKSA9PT0gLTEpIHtcbiAgICAgICAgZG9jdW1lbnQuZ2V0RWxlbWVudHNCeVRhZ05hbWUodGFnTmFtZSlbaV1bYXR0cl0gPSB3aW5kb3cubmd0aGVtZSArIHVybDtcbiAgICAgIH1cbiAgICB9XG4gIH1cblxuICBjaGFuZ2VVcmwoJ2ltZycsICdzcmMnKTtcbiAgY2hhbmdlVXJsKCdzY3JpcHQnLCAnc3JjJyk7XG4gIGNoYW5nZVVybCgnbGluaycsICdocmVmJyk7XG5cbiAgLy9zaWRlLWJhclxuICAkKCcubWVudScpLmNsaWNrKGZ1bmN0aW9uIChlKSB7XG4gICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICQodGhpcykudG9nZ2xlQ2xhc3MoJ2FjdGl2ZScpO1xuICAgIGlmICgkKHRoaXMpLmhhc0NsYXNzKCdhY3RpdmUnKSkge1xuICAgICAgJCh0aGlzKS5maW5kKCdpJykuYXR0cignY2xhc3MnLCAnZmFzIGZhLWFuZ2xlLXVwJyk7XG4gICAgfSBlbHNlIHtcbiAgICAgICQodGhpcykuZmluZCgnaScpLmF0dHIoJ2NsYXNzJywgJ2ZhcyBmYS1hbmdsZS1kb3duJyk7XG4gICAgfVxuICB9KVxuICAkKCcub3Blbi1tZW51LWl0ZW1zJykuY2xpY2soZnVuY3Rpb24gKGUpIHtcbiAgICBlLnByZXZlbnREZWZhdWx0KCk7XG4gICAgJCh0aGlzKS50b2dnbGVDbGFzcygnYWN0aXZlJyk7XG4gICAgaWYgKCQodGhpcykuaGFzQ2xhc3MoJ2FjdGl2ZScpKSB7XG4gICAgICAkKHRoaXMpLmZpbmQoJ2knKS5hdHRyKCdjbGFzcycsICdmYXMgZmEtYW5nbGUtdXAnKTtcbiAgICB9IGVsc2Uge1xuICAgICAgJCh0aGlzKS5maW5kKCdpJykuYXR0cignY2xhc3MnLCAnZmFzIGZhLWFuZ2xlLWRvd24nKTtcbiAgICB9XG4gIH0pXG5cbiAgLy8gbW9kYWxcblxuICAkKCcub3Blbi1tb2RhbCcpLmNsaWNrKGZ1bmN0aW9uIChlKSB7XG4gICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuICAgIHZhciBpZCA9ICQodGhpcykuYXR0cignaHJlZicpO1xuICAgICQoaWQpLmFkZENsYXNzKCdhY3RpdmUnKTtcbiAgfSk7XG5cbiAgJCgnLmNsb3NlJykuY2xpY2soZnVuY3Rpb24gKGUpIHtcbiAgICBlLnByZXZlbnREZWZhdWx0KCk7XG4gICAgJCgnLm1vZGFsJykucmVtb3ZlQ2xhc3MoJ2FjdGl2ZScpO1xuICB9KTtcblxuICAkKCcubW9kYWwnKS5jbGljayhmdW5jdGlvbiAoZSkge1xuICAgIGlmICgkKGUudGFyZ2V0KS5oYXNDbGFzcygnbW9kYWwnKSAmJiAkKGUudGFyZ2V0KS5oYXNDbGFzcygnYWN0aXZlJykpIHtcbiAgICAgICQoZS50YXJnZXQpLnJlbW92ZUNsYXNzKCdhY3RpdmUnKVxuICAgIH1cbiAgfSk7XG5cbiAvLyBtZW51XG5cbiAkKCcudG9nZ2xlIGEnKS5jbGljayhmdW5jdGlvbiAoZSkge1xuICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuICAgJCgnLmhlYWRlci1tZW51JykudG9nZ2xlQ2xhc3MoJ2FjdGl2ZScpO1xuIH0pXG59KTsiXSwiZmlsZSI6Im1haW4uanMifQ==

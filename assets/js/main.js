$(document).ready(function () {

  // change files url
  function changeUrl(tagName, attr) {

    for (var i = 0; i < document.getElementsByTagName(tagName).length; i++) {
      var url = document.getElementsByTagName(tagName)[i].getAttribute(attr);
      if (url && url.indexOf('http://') === -1 && url.indexOf('https://') === -1) {
        document.getElementsByTagName(tagName)[i][attr] = window.ngtheme + url;
      }
    }
  }

  changeUrl('img', 'src');
  changeUrl('script', 'src');
  changeUrl('link', 'href');

  //side-bar
  $('.menu').click(function (e) {
    e.preventDefault();
    $(this).toggleClass('active');
    if ($(this).hasClass('active')) {
      $(this).find('i').attr('class', 'fas fa-angle-up');
    } else {
      $(this).find('i').attr('class', 'fas fa-angle-down');
    }
  })
  $('.open-menu-items').click(function (e) {
    e.preventDefault();
    $(this).toggleClass('active');
    if ($(this).hasClass('active')) {
      $(this).find('i').attr('class', 'fas fa-angle-up');
    } else {
      $(this).find('i').attr('class', 'fas fa-angle-down');
    }
  })

  // modal

  $('.open-modal').click(function (e) {
    e.preventDefault();
    var id = $(this).attr('href');
    $(id).addClass('active');
  });

  $('.close').click(function (e) {
    e.preventDefault();
    $('.modal').removeClass('active');
  });

  $('.modal').click(function (e) {
    if ($(e.target).hasClass('modal') && $(e.target).hasClass('active')) {
      $(e.target).removeClass('active')
    }
  });

 // menu

 $('.toggle a').click(function (e) {
   e.preventDefault();
   $('.header-menu').toggleClass('active');
 })
});